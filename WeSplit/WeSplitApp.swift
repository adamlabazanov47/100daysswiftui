//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Адам Лабазанов on 25.11.2022.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
